#include<stdio.h>
struct employee
{
  char name[20];
  char id[20];
  char dob[20];
  char add[100];
};
int main ()
{
  int i, n;
  struct employee e[20];
  printf ("Enter the number of employees\n");
  scanf ("%d", &n);
  printf ("Enter the employees' name,id, date of birth and address\n");
  for (i = 0; i < n; i++)
    {
      scanf ("%s%s%s%s", e[i].name, e[i].id, e[i].dob, e[i].add);
    }
  printf ("The employee details are\n");
  for (i = 0; i < n; i++)
    {
      printf ("name:%s\nid:%s\ndate of birth:%s\naddress:%s\n", e[i].name,
	      e[i].id, e[i].dob, e[i].add);
    }
  return 0;
}