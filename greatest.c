#include<stdio.h>
int
input ()
{
  int a;
  printf ("Enter the number\n");
  scanf ("%d", &a);
  return a;
}

int
compute (int a, int b, int c)
{
  if (a > b && a > c)
    return a;
  else if (b > c && b > a)
    return b;
  else
    return c;
}

void
display (int g)
{
  printf ("The greatest of the three numbers is %d\n", g);
}

int
main ()
{
  int a, b, c, d;
  a = input ();
  b = input ();
  c = input ();
  d = compute (a, b, c);
  display (d);
  return 0;
}