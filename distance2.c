#include<stdio.h>
#include<math.h>
int input()
{
int x;
printf("Enter the x coordinate of the point\n");
scanf("%d",&x);
return x;
}
int input2()
{
int y;
printf("Enter the y coordinate of the point\n");
scanf("%d",&y);
return y;
}
int compute(int x1, int y1, int x2, int y2)
{
float d;
d=sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));
return d;
}
void display(int x1, int y1, int x2,int y2, float d)
{
printf("The distance between %d,%d and %d,%d is %f",x1,y1,x2,y2,d);
}
int main()
{
int x1,y1,x2,y2;
float d;
x1=input();
y1=input2();
x2=input();
y2=input2();
d=compute(x1,y1,x2,y2);
display(x1,y1,x2,y2,d);
return 0;
}