#include<stdio.h>
#define pi 3.14
float input()
{
    float r;
    printf("Enter the radius of the circle\n");
    scanf("%f",&r);
    return r;
}
float area(float x)
{
    float a;
    a=pi*x*x;
    return a;
}
float circum(float x)
{
    float c;
    c=2*pi*x;
    return c;
}
void output(float ar, float cir)
{
    printf("The area of the circle is %f\nThe circumference of the circle is %f\n",ar,cir);
}
int main()
{
    float r,a,c;
    r=input();
    a=area(r);
    c=circum(r);
    output(a,c);
    return 0;
}


