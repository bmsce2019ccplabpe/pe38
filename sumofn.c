#include<stdio.h>
int
input ()
{
  int n;
  printf ("Enter the value of n\n");
  scanf ("%d", &n);
  return n;
}

int
compute (int n)
{
  int i, sum = 0;
  for (i = 1; i <= n; i++)
    sum = sum + i;
  return sum;
}

void
output (int r, int x)
{
  printf ("The sum of %d natural numbers is %d\n", x, r);
}

int
main ()
{
  int n, res;
  n = input ();
  res = compute (n);
  output (res, n);
  return 0;
}